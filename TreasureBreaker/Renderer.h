//
//  Renderer.h
//  TreasureBreaker
//
//  Created by Heriberto Delgado on 3/4/19.
//  Copyright © 2019 Heriberto Delgado. All rights reserved.
//

#import <GVRKit/GVRKit.h>

@interface Renderer : GVRRenderer

-(void)updateTransform:(simd_float4x4)transform isTracking:(BOOL)isTracking;

@end
