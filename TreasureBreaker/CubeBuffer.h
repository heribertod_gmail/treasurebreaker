//
//  CubeBuffer.h
//  TreasureBreaker
//
//  Created by Heriberto Delgado on 3/4/19.
//  Copyright © 2019 Heriberto Delgado. All rights reserved.
//

#import "Buffer.h"

@interface CubeBuffer : Buffer

-(BOOL)isFocusedInDirection:(GLKVector4)direction atPosition:(GLKVector4)position andCubePosition:(GLKVector3)cubePosition;

@end
