//
//  FloorProgram.h
//  TreasureBreaker
//
//  Created by Heriberto Delgado on 3/4/19.
//  Copyright © 2019 Heriberto Delgado. All rights reserved.
//

#import "Program.h"

@interface FloorProgram : Program

@property (assign, nonatomic) GLint vertex;

@property (assign, nonatomic) GLint color;

@property (assign, nonatomic) GLint transform;

@property (assign, nonatomic) GLint position;

@end
